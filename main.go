package main

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/buger/jsonparser"
	"github.com/go-redis/redis"
)

const PORT_NUMBER = 80
const DEFAULT_DURATION = 30 // in second
const MAX_ATTEMPT = 3
const KEY_ITRA = "ITRA"
const KEY_SUSP = "SUSP"

func main() {
	//some info not hurting
	fmt.Println("==== OTP services ====")
	fmt.Println(fmt.Sprintf(" Run on Port :%d", PORT_NUMBER))
	fmt.Println("======================")
	rdb := conn()
	//close when not needed never trust Garbage Collector
	defer rdb.Close()
	mux := http.NewServeMux()
	mux.HandleFunc("/apiv1/digiroin/otp", otp(rdb))
	mux.HandleFunc("/apiv1/digiroin/check", checkOtp(rdb))
	http.ListenAndServe(fmt.Sprintf(":%d", PORT_NUMBER), mux)
}

func conn() *redis.Client {
	client := redis.NewClient(&redis.Options{
		//Addr: "localhost:6379",
		Addr:     "35.194.1.0:6379",
		Password: "t3guhCakep", // no password set
		DB:       5,  // use default DB
	})
	return client
}


// OTP DENGAN CHECK EXISTING USER
//return a funct more fast beause function loaded in memory on the first place
func otps(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		client := &http.Client{}
		code := http.StatusBadRequest
		//formating text fastest then + operation
		message := fmt.Sprintf(`{"code":%d,"message":"Error : Bad Request"}`, code)
		body, _ := ioutil.ReadAll(r.Body)
		//always use lowercase for key in json
		pubKey, err := jsonparser.GetString(body, "publickey")
		phone, err := jsonparser.GetString(body, "phone")
		//selalu gunakan single call, memory ringan dan cepat
		if len(pubKey) > 0 && len(phone) > 6 {
			var jsonprep string = `{"address":"` + pubKey + `","phone":"` + phone + `"}`
			var jsonStr = []byte(jsonprep)
			req, _ := http.NewRequest("POST", "http://128.199.186.186/apiv1/phone-check", bytes.NewBuffer(jsonStr))
			req.Header.Set("Content-Type", "application/json")
			resp, _ := client.Do(req)
			if resp.StatusCode == 409 {
				code = http.StatusConflict
				message = fmt.Sprintf(`{"code":%d,"message":"Nomor telah terdaftar"}`, code)
			} else {
				keys := fmt.Sprintf("%s:%s", pubKey, phone)
				_, err = rdb.Get(keys).Result()
				if err == redis.Nil {
					tempOtp := strconv.FormatUint(random(), 10)
					err = rdb.Set(keys, tempOtp, time.Minute*time.Duration(5)).Err()
					if err != nil {
						//formating text fastest then + operation
						message = fmt.Sprintf(`{"code":%d,"message":"Service unavailable"}`, code)
					}
					code = http.StatusOK
					message = fmt.Sprintf(`{"code":%d,"message":"Success"}`, code)
					//no need to make sure sms dikirim fire and forget aja toh bisa minta lagi dalam 5menit
					sms(tempOtp, phone)
				} else {
					//pemanfaatan code http sesuai dengan error / problem
					code = http.StatusConflict
					//formating text fastest then + operation kaena tidak ada copy antar memory space
					message = fmt.Sprintf(`{"code":%d,"message":"OTP sudah terkirim, Mohon tunggu 5 menit untuk request OTP baru"}`, code)
				}
			}
		}
		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func checkOtps(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		client := &http.Client{}
		code := http.StatusBadRequest
		body, _ := ioutil.ReadAll(r.Body)
		pubKey, _ := jsonparser.GetString(body, "publickey")
		phone, _ := jsonparser.GetString(body, "phone")
		token, _ := jsonparser.GetString(body, "token")
		keys := fmt.Sprintf("%s:%s", pubKey, phone)
		val, err := rdb.Get(keys).Result()

		if err == redis.Nil || val != token {
			code = http.StatusNotFound
		} else {
			var jsonprep string = `{"address":"` + pubKey + `","phone":"` + phone + `"}`
			var jsonStr = []byte(jsonprep)
			req, _ := http.NewRequest("POST", "http://128.199.186.186/apiv1/phone", bytes.NewBuffer(jsonStr))
			req.Header.Set("Content-Type", "application/json")
			resp, _ := client.Do(req)
			if resp.StatusCode == http.StatusOK || resp.StatusCode == http.StatusCreated || resp.StatusCode == http.StatusAccepted {
				code = http.StatusOK
			} else {
				code = http.StatusServiceUnavailable
			}
		}
		//no need message jika udah bsia diwakili sama return code cuman membutuhkan sekali jalan hemat network
		w.WriteHeader(code)
	}
}

// OTP TANPA CHECK EXISTING USER
func otp(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		code := http.StatusBadRequest
		//formating text fastest then + operation
		message := fmt.Sprintf(`{"code":%d,"message":"Error : Bad Request"}`, code)
		body, _ := ioutil.ReadAll(r.Body)
		//always use lowercase for key in json
		phone, _ := jsonparser.GetString(body, "phone")
		duration, _ := jsonparser.GetInt(body, "duration")
		if duration == 0{
			duration = DEFAULT_DURATION
		}
		//selalu gunakan single call, memory ringan dan cepat
		if len(phone) > 6 {
			//save the attempt of req OTP
			keyItr := fmt.Sprintf("%s:%s",KEY_ITRA,strings.Trim(phone," "))
			//status suspended phone number
			keyExp := fmt.Sprintf("%s:%s",KEY_SUSP,strings.Trim(phone," "))
			strTimeUnix, err := rdb.Get(keyExp).Result()
			if err == redis.Nil {
				val, err := rdb.Get(keyItr).Result()
				intVal,_ := strconv.Atoi(val)
				if err == redis.Nil || intVal < MAX_ATTEMPT {
					_, err = rdb.Get(phone).Result()
					if err == redis.Nil {
						tempOtp := strconv.FormatUint(random(), 10)
						err = rdb.Set(phone, tempOtp, time.Second*time.Duration(duration)).Err()
						if err != nil {
							code = http.StatusInternalServerError
							//formating text fastest then + operation
							message = fmt.Sprintf(`{"code":%d,"message":"Service unavailable"}`, code)
						}else{
							code = http.StatusOK
							message = fmt.Sprintf(`{"code":%d,"message":"Success"}`, code)
							//no need to make sure sms dikirim fire and forget aja toh bisa minta lagi dalam 5menit
							sms(tempOtp, phone)
							strItr := strconv.Itoa(intVal+1)
							err = rdb.Set(keyItr, strItr, time.Hour*time.Duration(24)).Err()
							if err != nil {
								code = http.StatusInternalServerError
								//formating text fastest then + operation
								message = fmt.Sprintf(`{"code":%d,"message":"Service unavailable"}`, code)
							}
						}
					} else {
						//pemanfaatan code http sesuai dengan error / problem
						code = http.StatusConflict
						//formating text fastest then + operation kaena tidak ada copy antar memory space
						message = fmt.Sprintf(`{"code":%d,"message":"OTP sudah terkirim, Mohon tunggu %d detik untuk request OTP baru"}`, code, duration)
					}
				}else{
					timein := time.Now().Local().Add(time.Hour * time.Duration(24) +
						time.Minute * time.Duration(0) +
						time.Second * time.Duration(0))
					tm := timein.Unix()
					err = rdb.Set(keyExp, tm, time.Hour*time.Duration(24)).Err()
					if err != nil {
						code = http.StatusInternalServerError
						//formating text fastest then + operation
						message = fmt.Sprintf(`{"code":%d,"message":"Service unavailable"}`, code)
					}else{
						//pemanfaatan code http sesuai dengan error / problem
						code = http.StatusForbidden
						//formating text fastest then + operation kaena tidak ada copy antar memory space
						message = fmt.Sprintf(`{"code":%d,"message":"Nomor Anda Telah Melewati %d Kali Percobaan OTP, Mohon tunggu 1 x 24 jam untuk request OTP baru","retryTime":%d}`, code, MAX_ATTEMPT,tm)
					}
					rdb.Del(keyItr)
				}
			}else{
				//pemanfaatan code http sesuai dengan error / problem
				code = http.StatusForbidden
				//formating text fastest then + operation kaena tidak ada copy antar memory space
				message = fmt.Sprintf(`{"code":%d,"message":"Nomor Anda Telah Melewati %d Kali Percobaan OTP, Mohon tunggu 1 x 24 jam untuk request OTP baru","retryTime":%s}`, code, MAX_ATTEMPT,strTimeUnix)
				rdb.Del(keyItr)
			}
		}
		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func checkOtp(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		code := http.StatusBadRequest
		body, _ := ioutil.ReadAll(r.Body)
		phone, _ := jsonparser.GetString(body, "phone")
		token, _ := jsonparser.GetString(body, "token")
		val, err := rdb.Get(phone).Result()
		if err == redis.Nil || val != token {
			code = http.StatusNotFound
		} else {
			code = http.StatusOK
			rdb.Del(phone)
			rdb.Del(fmt.Sprintf("%s:%s",KEY_ITRA,phone))
		}
		//no need message jika udah bsia diwakili sama return code cuman membutuhkan sekali jalan hemat network
		w.WriteHeader(code)
	}
}


func sms(otp string, phone string) (err error) {

	text := "Pengen Aktivasi Digiroin? Masukan 4 Digit " + otp + " ini dulu ya!."
	messages := strings.Replace(text, " ", "%20", -1)
	url := "http://sms-api.jatismobile.com/index.ashx?channel=2&uploadby=test&batachname=test&division=OPERASI%20DAN%20LAYANAN%20TEKNOLOGI&sender=KANTOR%20POS&message=" + messages + "&msisdn=" + phone + "&password=POSINDO879&userid=POSINDO"
	req, _ := http.NewRequest("GET", url, nil)
	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	message := string(body)
	firstSplit := strings.Split(message, "&")
	secSplit := strings.Split(firstSplit[0], "=")
	status := secSplit[1]

	if strings.Compare(status, "1") > 0 {
		err = errors.New("Error Send SMS")
	}

	return err
}

func random() uint64 {
	rand.Seed(time.Now().Unix())
	val := uint64(rand.Intn(9999-1000) + 1000)
	return val
}
